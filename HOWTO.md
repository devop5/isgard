# Quick Start

It is envisaged that creating a new Domain will be quite uncommon, whilst adding / removing / updating individual
 environments within that **domain** is less so.  This Quick Start is geared towards the latter scenario.

## Prerequisites


It is assumed that:

  - a **domain** has already been created and tested
  - the user has been given an iAM user + credentials, with the appropriate policies.
    * *TODO* Provide a structure iam template for all the above. Or code-ify in terraform.
  - It is best-practice to create a separate IAC user for each team member using this tool, 
    with the the minimum permissions required.



## Instructions

## Create a domain

A domain contains many test environments.
A domain's name matches the DNS suffix. ie. 'test.devop5.io'

1. `git clone` this folder
2. Install terraform
3. Update your secrets (TODO)

Changing an environment: (Assuming an environment already exists (90% of this tool's usage))

1.  Make required changes to infrastructure in *defs/testenv*

2.  Preview the changes: `./iac plan`

    If this looks bad (ie. huge number of things changing, then stop here and check)

3.  Implement the changes: `./iac apply`

4.  Smoketest the changes: `./iac test`
   
    This runs a very quick smoketest to check things are working correctly



