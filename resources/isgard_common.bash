#!/bin/bash


creset=$(tput sgr0)
cred=$(tput setaf 1)
cblue=$(tput setaf 4)
cgreen=$(tput setaf 2)



check_file() {
    if [ ! -f $1 ]; then
        echo "ERROR: Could not find $argv"
        echo "       See README.md for more details."
        return 1
    fi
    return 0
}



setenv() {
    export $1=$2
}


# Converts simple hierarchical yaml to blah_level2_level3="blah" statements
# From: http://stackoverflow.com/a/21189044/3153192
#
# Usage:
#    parse_yaml $PATH          - generates x_y="blah" stmts
#    parse_yaml $PATH aaaa     - generates aaaa_x_y="blah" stmts
# A nice hack to generate 'local x_y="blah"' statements:
#    parse_yaml $PATH "__local__" | sed 's/__local__/local /'
# Use the above with an eval to generate local variables within a bash function
parse_yaml() {
   local prefix=${2:-}
   local s='[[:space:]]*' w='[a-zA-Z0-9_]*' fs=$(echo @|tr @ '\034')

   sed -ne "s|^\($s\):|\1|" \
        -e "s|^\($s\)\($w\)$s:$s[\"']\(.*\)[\"']$s\$|\1$fs\2$fs\3|p" \
        -e "s|^\($s\)\($w\)$s:$s\(.*\)$s\$|\1$fs\2$fs\3|p"  $1 |
   awk -F$fs '{
      indent = length($1)/2;
      vname[indent] = $2;
      for (i in vname) {if (i > indent) {delete vname[i]}}
      if (length($3) > 0) {
         vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
         printf("%s%s%s=\"%s\"\n", "'$prefix'",vn, $2, $3);
      }
   }'
}


# Takes the given file in "terraform output" format, and stdout's into .tfvar compatible strings (for inputting back into Terraform.)
#
# Input:
#   A file containing:
#       sg_aaa_id = sg-74172111
#       sg_bbb_id = "sg-74172112"
#       sg_ccc_id=sg-6b17210e
#       sg_ddd_id=4
# Output:
#   stdout:
#       sg_aaa_id = "sg-74172111"
#       sg_bbb_id = "sg-74172112"
#       sg_ccc_id = "sg-6b17210e"
#       sg_ddd_id = "4"
#
# Notes:
#       - Idempotent, so can safely ignore ""
#       - This function has the highest comment:code ratio!
#       - caveat: Not sure how well it'll work with complex data structures (but are any being output anyway?)
#
# Alternative Mechanism:
#   1) Quick hack that converts the = into : (ie. yaml-like) and then run through the parse_yaml()?
#   2) Wait for: https://github.com/hashicorp/terraform/issues/1209
#             or https://github.com/hashicorp/terraform/issues/2460
#
#
convert_to_tfvars() {
    local thefile="${1:-}"
    if [[ -z ${thefile} ]] || [[ ! -f ${thefile} ]]; then
        echo "Cannot find file: ${thefile}"
        return 1
    fi
    # 2nd word is "\w[^"]*" instead of the far preferable "\w*" due to the dashes
    sed -r 's/^(\w+) *= *(\w[^"]*)$/\1 = "\2"/' ${thefile}  # outputs to stdout - the callee decides what to do
}
