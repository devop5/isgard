#!/bin/bash

### BOILERPLATE STARTS     -  http://kvz.io/blog/2013/11/21/bash-best-practices/
set -o nounset      # Force variable safety (strict mode)
set -o errexit      # Finish script on any error (add '|| true' to bypass)
set -o pipefail     # Avoid masked exitcodes
#set -o xtrace      # Moved to main()

# Set magic variables for current file & dir
__IACDIR="$(cd "$(dirname $(realpath "${BASH_SOURCE[0]}"))" && pwd)"
__FILE="${__IACDIR}/$(basename "${BASH_SOURCE[0]}")"
__BASE="$(basename ${__FILE} .sh)"
__OWD="$(pwd)"          # Original Working Directory - from which the script/symlink was run
readonly __IACDIR __FILE __BASE __CWD
### BOILERPLATE ENDS



source "${__IACDIR}/resources/isgard_common.bash"

# relative paths within the iac tool
path_blueprints="farm/blueprints"         # aka. defs
path_seeds="farm/seeds"             # aka. domain_bases
path_gardens="farm/gardens"         # aka. envs


# Initially, it seemed a good idea to allow the AWS values to come from the environment.
#   ie. a user has ONE secrets, and a domain has ONE secret.
# However, we realised that the AWS-values could be different for each domain.
#   ie. A user has MANY secrets (one per domain), a domain could have MANY secrets (but only one for each user)
# Therefore, clearing these values now (but not exporting the cleared value) to avoid ambiguity.
AWS_DEFAULT_REGION=""
AWS_ACCESS_KEY_ID=""
AWS_SECRET_ACCESS_KEY=""
TERRAFORM_S3_BUCKET=""


#
# These are 'globals' - usually set to readonly after the value is determined
# (helps avoid side-effects from the NeedingGlobalsToWriteScalableBash challenge)
#
verbose=""          # set in main()
domain=""           # The name (and DNS suffix) of the domain (see domain_path for location)
env=""              # The name of the environment (see env_path for location)
domain_short=""     # A friendlier version of the domain for tagging. eg. "blah.domain.local"->"blah-domain"
domain_path=""      # Relative path to the domain.      eg. "farm/gardens/xyz.domainname.local"
env_path=""         # relative path to the environment. eg: "farm/gardens/xyz.domainname.local/envname"
secret_path=""      # Symlink (normally located at env/my_secrets) to the secrets file (which can be located anywhere)

#
# Globals that shouldn't be (refactor to params later)
#
srcsecret_path=""   # Only used in createdomain. Specifies the path to the actual secret file, rather than the symlinks that are generally used
domain_seed=""      # Only used in createdomain. Specifies the base template for a new domain.




show_domain_seeds() {
    echo "Available domain seeds:"
    for B in "${path_seeds}/"*; do
        if [ -d ${B} ]; then
            echo -e -n "  $(basename ${B})   \t- \t"
            [ -f "${B}/description.txt" ] && cat "${B}/description.txt" || echo "No description"
        fi
    done
}


show_help() {
    # Outputs one of:
    #   $1 = null    : short help - common commands
    #   $1 = help    : long help  - all commands
    #   $1 = command : help on specific command


    # Asking for 'help' gives full help, otherwise only 'short' help (common commands)
    local help_arg=${1:-short}
    local help_verbose=0
    [[ ${help_arg} = "short" ]] && help_verbose=1
    [[ ${help_arg} = "help" ]] && help_verbose=2

    # Print out the required/requested help
    if [[ ${help_verbose} = 1 ]]; then
        echo "Basic commands:"
    elif [[ ${help_verbose} = 2 ]]; then
        echo "All commands:"
    else
        echo "Usage:"
    fi

    if [[ ${help_verbose} > 0 ]]; then
        echo "    ${__BASE} help [<DOMAIN>]"
        echo "            Shows advanced commands"
    fi

    # Commands
    if [[ ${help_verbose} > 0 || ${help_arg} = "show" ]]; then
        echo "    ${__BASE} [-v] show [<DOMAIN>]"
        echo "            Shows all domains managed by IAC, or details on a specific domain"
    fi
    if [[ ${help_verbose} > 0 || ${help_arg} = "register" ]]; then
        echo "    ${__BASE} register <DOMAIN> <SECRET_FILE>"
        echo "            Registers the secrets to be used for a specific domain. Only needed if the current user was not the"
        echo "            original domain creator. These secrets are not stored in source control, and are user-specific."
    fi
    if [[ ${help_verbose} > 0 || ${help_arg} = "create" ]]; then
        echo "    ${__BASE} create <DOMAIN> <ENV>"
        echo "            Adds a managed environment to an existing domain. ENV is the prefix to the DOMAIN's suffix."
    fi
    if [[ ${help_verbose} > 0 || ${help_arg} = "todo" ]]; then
        echo "    ${__BASE} todo <DOMAIN>"
        echo "            Shows incomplete manual configuration for DOMAIN."
    fi
    if [[ ${help_verbose} > 0 || ${help_arg} = "plan" ]]; then
        echo "    ${__BASE} plan <DOMAIN> <ENV>"
        echo "            Plans the specified ENV within a DOMAIN. To plan the core environment use 'core'."
        echo "            Note: The ability to terraform multiple environments within a DOMAIN is deliberately not implemented yet."
    fi
    if [[ ${help_verbose} > 0 || ${help_arg} = "apply" ]]; then
        echo "    ${__BASE} apply <DOMAIN> <ENV>"
        echo "            Applies the specified ENV within a DOMAIN. To apply the core environment use 'core'."
        echo "            Note: The ability to terraform multiple environments within a DOMAIN is deliberately not implemented yet."
    fi
    if [[ ${help_verbose} > 1 || ${help_arg} = "createdomain" ]]; then
        echo "    ${__BASE} [-v] createdomain  <DOMAIN> <DOMAIN_SEED> <SECRETS_FILE>"
        echo "            Creates a managed domain (a domain = CORE env + multiple test-ENVs) within a directory tree,"
        echo "            and sets up shared tf states. This only needs to be done once per domain (ie. very rarely)."
        echo "            Parameters: "
        echo "                   DOMAIN        The name of the domain. Also the DNS suffix for all contained environments."
        echo "                   DOMAIN_SEED   A pre-defined template of variables for this domain. eg. CIDRs, VPCs"
        echo "                   SECRETS       Secrets not saved in git but are required for initialisation."
        echo "    ${__BASE} createdomain -l"
        echo "            Show available domain seeds."
    fi
    if [[ ${help_verbose} > 1 || ${help_arg} = "load" ]]; then
        echo "   ${__BASE} custom <DOMAIN> <ENVIRONMENT> <TERRAFORM_COMMAND>"
        echo "            Runs any terraform command within the specified domain and environment, after setting the appropriate secrets"
    fi
    if [[ ${help_verbose} > 1 || ${help_arg} = "destroy" ]]; then
        echo "   ${__BASE} destroy <DOMAIN> <ENVIRONMENT>"
        echo "            Destroys the specified environment, within the specified domain."
    fi

    if [[ ${help_verbose} > 1 ]]; then
        echo
        echo "Extra arguments:   (added before the command)"
        echo "    -v     Provides some extra verbose output."
        echo "    -d     Debug output (spam!)"
        echo
        echo "Implicit mode:"
        echo "      A symlink to this script is created within each <DOMAIN>"
        echo "      This symlink can be used to run the iac script, and the domain+env are implicitly determined based on the "
        echo "      current working directory, avoiding the need to specify the domain and environment."
        echo "      ie. Commands with both DOMAIN and ENVIRONMENT can avoid both. eg:"
        echo "         ${__FILE} plan,   ${__FILE} apply,   ${__FILE} custom"
        echo "      (But not creation or global commands)"
        echo

        echo "NOTE:"
        echo "  - Secret files are not stored in source control, and can be different for each domain (allowing FGAC)."
        echo "    Thus, each user must provide their secrets before operating on a domain."
        echo "  - Copy the existing secrets.template.yaml to secrets_<XXX>.yaml"
        echo "    Suggest something like 'secrets_domainnickname.yaml' to allow for different secrets per domain."
    fi

    # save for later...
    if false; then
        echo "Where: (see README.md for more information)"
        echo "  DOMAIN         = The name of the desired group of environments. ie. the base environment and multiple test-envs."
        echo "  BASE_ENV       = Pre-populated env variables. This includes the domain suffix (eg. test1.devop5.io)"
        echo "                    It is likely that the created var-file will need to be edited."
        echo "                    Valid values: $(show_domain_seeds)"
        echo "  SECRETS_FILE    = the secrets file to symlink to (symlink helps avoiding adding this file into git!)"

        echo
        echo "All AWS resources are tagged with the values above."
    fi

}


_load_secrets() {
    # Inputs
    #   $1 - path to the secrets to load
    # Outputs (global)
    #   AWS_DEFAULT_REGION
    #   AWS_ACCESS_KEY_ID
    #   AWS_SECRET_ACCESS_KEY
    #   TF_VAR_aws_access_key_id
    #   TF_VAR_aws_secret_access_key

    local arg_secrets=${1:-}

    # Validation / Checking
    [ -z ${arg_secrets} ] && echo "Secret file not specified" && return 1
    [ ! -f ${arg_secrets} ] && echo "Cannot find secret file: ${arg_secrets}" && return 1

    #eval $(parse_yaml ${secrets_to_load})
    for kv in $(parse_yaml ${arg_secrets}); do
        # need to strip the quotes.... eg. aws_secret="blah" -> aws_secret=blah
        local kv2=$(echo ${kv} | sed -E 's/="([^"]*)"/=\1/')
        # Export, so available to external programs (eg. terraform)
        export ${kv2}
    done

    if [ -z ${AWS_DEFAULT_REGION:-} ]; then
        echo "WARNING: Secrets file fails to contain AWS_DEFAULT_REGION\n"
    fi
    if [ -z ${AWS_ACCESS_KEY_ID:-} ]; then
        echo "WARNING: Secrets file fails to contain AWS_ACCESS_KEY_ID\n"
    fi
    if [ -z ${AWS_SECRET_ACCESS_KEY:-} ]; then
        echo "WARNING: Secrets file fails to contain AWS_SECRET_ACCESS_KEY\n"
    fi

    # Setting AWS vars from secret vars
    export TF_VAR_aws_access_key_id="${AWS_ACCESS_KEY_ID}"
    export TF_VAR_aws_secret_access_key="${AWS_SECRET_ACCESS_KEY}"
    #export TF_VAR_terraform_s3_bucket="${TERRAFORM_S3_BUCKET:-}"
}



_iac_load_domain() {
    ## Used (indirectly) by most commands.
    ## Inputs: (globals)
    ##      domain  (validated by this function)
    ## Outputs: (globals)
    ##      domain_path
    ##      secret_path
    ## Normally the domain is checked for existence. However, if the domain shouldn't exist yet,
    ## the non-existence can be validated:
    ##      $1 = no_domain
    if [ -z "${domain}" ]; then
        echo "  Missing DOMAIN: Must specify a domain"
        return 1
    fi
    # Remove the .local or .io, and then substitute .'s for -'s.
    domain_short="$(echo "${domain%.*}" | sed -r -e 's/\./\-/g')"
    domain_path="${path_gardens}/${domain}"

    # special case: remove -devop5 for nicer tags in our VPC
    domain_short="${domain_short%-devop5}"


    if [[ "${1:-}" = "no_domain" ]]; then
        # There must NOT be an existing environment
        [ -d ${domain_path} ] && echo "Pre-existing domain found at: ${domain_path}" && return 1
    fi
    return 0
}



_iac_load_env() {
    ## Used (indirectly) by most commands.
    ## Inputs: (globals)
    ##      domain
    ##      env             - validated by this function
    ## Outputs:
    ##      Globals:
    ##          env_path
    ##          domain_short
    ##          secret_path
    ##          env_type
    ##      Via _iac_load_domain():
    ##          See _iac_load_domain outputs
    ##      Via _load_secrets():
    ##          AWS_DEFAULT_REGION
    ##          AWS_ACCESS_KEY_ID
    ##          AWS_SECRET_ACCESS_KEY
    ##          TF_VAR_aws_access_key_id
    ##          TF_VAR_aws_secret_access_key
    ##
    ## If env shouldn't exist yet (eg. for a env-creation call), then reverse the env-directory checking with:
    ##      $1 = no_env

    _iac_load_domain || return 1

    if [ -z "${env}" ]; then
        echo "  Missing ENV: Must specify an environment"
        return 1
    fi


    env_path="${domain_path}/${env}"
    env_type="$( [[ ${env} = "core" ]] && echo 'core' || echo 'sub' )"  # 'core' or 'sub'


    # Testing to see if domain/env/ exists?
    # Under some circumstances (specified with the "no_env" parameter") the opposite is tested. eg. creating new env.
    arg=${1:-}
    if [[ "${arg}" != "no_env" ]]; then
        # There must be an existing environment
        [ ! -d ${env_path} ] && echo "No environment found at: ${env_path}" && return 1

        # Use the env's secrets
        secret_path="${env_path}/my_secrets"
    else
        # There must NOT be an existing environment
        [ -d ${env_path} ] && echo "Existing environment found at: ${env_path}" && return 1

        # Use the domain's secrets
        secret_path="${domain_path}/my_secrets"
    fi

    if ! _load_secrets ${secret_path} ; then
        echo "Could not load secret file"
        return 1
    fi

    # lock the values down.....
    # Excessive? IMHO, makes coding with globals safer
    readonly domain_path env_path env_type secret_path
}


_iac_set_domainenv_from_cwd() {
    # Test to see if the user is in a ".../farm/gardens/DOMAIN/ENV" directory, and if so, derive the domain and env implicitly.
    # Outputs:
    #
    #   EITHER
    #       domain  (global)
    #       env     (global)
    #       returncode=0
    #   OR
    #       returncode=1    (fail)

    [ ${verbose} ] && echo "IAC Directory (__IACDIR) = ${__IACDIR}"
    [ ${verbose} ] && echo "Original working dir (__OWD) = ${__OWD}"
    [ ${verbose} ] && echo "Current working dir (PWD)   = ${PWD}"

    # Is the PWD under the garden directory? If not, then use parameters.
    local found_values=""
    if [[ "${__OWD}" != "${__IACDIR}/farm/gardens/"* ]]; then
        [ ${verbose} ] && echo "We are not under farm/gardens/, so cannot perform set domain & env implicitly"
        return 1
    fi

    # Appears we are under env. Try to determine domain/env
    local trial_env="$(basename ${__OWD})"
    local trial_domain="$(basename $(dirname ${__OWD}))"

    if [[ ${trial_domain} = "gardens" ]]; then
        # We aren't deep enough. ie. we are in a domain, not a env. give up.
        #   ie. PWD=.../farm/gardens/aaa.devop5.io/ (domain).
        #   vs. PWD=.../farm/gardens/aaa.devop5.io/qa7/ (env)
        [ ${verbose} ] && echo "Current directory is a _domain_. Cannot implicit set domain & env."
        return 1
    fi

    # now validate they are actually domains and envs (probably paranoid overchecking, but... anyway...)
    trial_dir="farm/gardens/${trial_domain}/${trial_env}"
    if [ ! -d "${trial_dir}" ]; then
        [ ${verbose} ] && echo "Implicit domain/env setting should've worked, but failed. Reverting to explicit."
        return 1
    fi

    # looks good!
    if [ ${verbose} ]; then
        echo "Based on working directory, setting:"
        echo "   domain = ${trial_domain}"
        echo "   env    = ${trial_env}"
    fi
    domain=${trial_domain}
    env=${trial_env}
    readonly domain env # lockdown

    return 0
}

_iac_todo_execute() {
#    # Validation
#    if [ -z ${domain} ]; then
#        echo "ERROR: Cannot show TODO's. Domain not specified"
#        show_help todo
#        return 1
#    fi

    [ ! -d ${domain_path} ] && echo "ERROR: Cannot show TODO's. Cannot find domain at '${domain_path}' " && return 1

    local instruction_prefix="Config requiring manual editing:" ;
    #grep -r -T --color=always --label=' 123 123 ' --no-group-separator "IAC_TODO" ${domain_path}
    for F in $(grep -l -r  "IAC_TODO" ${domain_path}); do
        echo "${instruction_prefix} ${F}"
        grep --color=ALWAYS 'IAC_TODO' ${F} | while read -r L; do
            echo "    ${L}"
        done
        echo
    done
}


iac_todo() {
    # Shows all outstanding IAC_TODOs for the specific domain
    domain=${1:-}
    _iac_load_domain || return 1
    _iac_todo_execute
}


_iac_show_domain() {
    # Shows the domain specified in $1.
    # If ${verbose} global is set, then show more detail.

    local domain=${1:-}
    _iac_load_domain || return 1

    if ! which tree > /dev/null; then
        echo "ERROR: Cannot find 'tree' command. Please install."
        return 1
    fi

    # display
    local treeargs="$( [ ${verbose} ] && echo '--noreport  -a -C -F' || echo '--noreport -C -F -L 1' )"
    tree ${treeargs} ${domain_path}
}



iac_show() {
    # Show either a specified domain, or all domains
    local arg=${1:-}

    # show one domain
    if [ ! -z ${arg} ]; then
        _iac_show_domain "${arg}"
        return
    fi

    # Show all domains
    #find env/ -maxdepth 1 -mindepth 1 -type d | while read -r D; do
    for D in "farm/gardens/"*; do
        # use the presence of core/ to determine if this is valid or not
        [ -d "${D}/core" ] && _iac_show_domain "${D#*gardens/}"  # discard the directories above
        echo
    done
}


iac_custom_command() {
    # Used for semi-manual terraformming.
    # Sets the secrets and runs the command in the domain/environment.
    if ! _iac_set_domainenv_from_cwd; then
        readonly domain=${1:-}
        readonly env=${2:-}
        shift 2
    fi
    _iac_load_env

    echo
    echo -n "NOTE: You may wish to append the options: -var-file=domain.tfvars -var-file=env.tfvars "
    [[ ${env} != "core" ]] && echo "-var-file=core.tfvars" || echo
    echo

    cd "${env_path}"
    terraform "$@"
    cd "${__IACDIR}"

}


_iac_create_validate() {
    # Validation Phase 2: Semantics - paths & secrets
    local arg_error=""
    if [ ! -d  ${domain_path} ]; then
        arg_error="  Domain '${domain}' does not exist\n"
    elif [ ! -s ${secret_path} ]; then
        arg_error="  Could not find secrets. Please register your secrets. See './iac.sh help register' for more information'"
    elif ! _load_secrets "${secret_path}"; then
        arg_error="  Could not load secret file: ${secret_path}"
    elif [ -d ${env_path} ]; then
        arg_error="  Environment '${env}' already exists at '${env_path}'"
    fi

    # Show error, show help and exit.
    if [ ! -z "${arg_error}" ]; then
        echo "ERROR:"
        echo -e "${arg_error}"
        show_help create
        return 1
    fi
    return 0
}

_iac_create_execute() {
    ## Creates a new domain
    ##
    ## Inputs: (globals)
    ##  domain
    ##  env
    ## Outputs:
    ##  If successful:
    ##    1. a new environment for the specified domain (at gardens/${domain}/core)

    echo
    echo "New Environment:            ${domain}/${env}"
    #echo
    # TODO: add [y/N] confirmation

    # Get the appropriate environment template (either 'sub' or 'core')
    local env_template_path="${path_blueprints}/domain_templates/env_${env_type}"

    # Phase 1 - setup terraform remote
    mkdir "${env_path}"

    # Need to changed the CWD. Otherwise .terraform/ is created in the iac/ root
    cd "${env_path}"
    # Create the template environment
    echo -n '    '
    if ! terraform init \
        -backend=s3 \
        -backend-config="bucket=${TERRAFORM_S3_BUCKET}" \
        -backend-config="key=tfstate/${domain}/${env}/project.json" \
        -backend-config="acl=bucket-owner-full-control" \
        -backend-config="state=terraform.tfstate" \
        "../../../../${env_template_path}" \
        ./ ; then
        echo "Non-Zero error returned from terraform init. Halting; try -t[wid] for more info."
        return 1
    fi
    cd "${__IACDIR}"

    # Substitute the environment's name into the main.tf template
    if [[ "${env_type}" != "core" ]]; then
        sed -e "s/__IAC_ENV__/${env}/" -i ${env_path}/main.tf
    fi



    # Phase 2 - complete the rest of the structure
    #   1. by linking from domain/vars into domain/env/
    #   2. copying remaining template files
    #   3. linking secrets
    # and copy most of the files (except the 'terraform init' sourced above)
    # link var files (except for env_core or env_sub which are done separately)
    for F in ${domain_path}/vars/*.tf ${domain_path}/vars/*.tfvars; do
        ln -s -r ${F} ${env_path}/
    done
    # copy tfvars files
    cp ${env_template_path}/env.tfvars ${env_path}/
    # Substitute the environment's name into the main.tf template
    sed -e "s/__IAC_ENV_NAME__/${env}/" -i ${env_path}/env.tfvars

    # link secrets, etc
    ln -s  "../my_secrets" "${env_path}/"             # Linking to symlink. Less to change if required.
    ln -s -r "${path_blueprints}/modules/" "${env_path}/"                   # link terraform modules
    ln -s -r "${__FILE}" "${env_path}/${__BASE}"      # link to iac script (for implicit mode)

    if [[ "${env_type}" != "core" ]]; then
        # link core deps (in case it doesn't exist yet - eg. qa7 created before core was applied)
        [ ! -L "${env_path}/core.tfvars" ] && ln -s -r "${domain_path}/vars/core.tfvars" "${env_path}/"
    fi


    echo "    Complete!"

    # Do an initial push
    cd "${env_path}"
    echo -n "    "
    terraform remote push || return 1
    cd "${__IACDIR}"

    # Show todo
    echo
    _iac_todo_execute ${domain} || return 1

    return 0
}

iac_create() {
    ## Wraps the separate phases of validate & execute, after parsing args as globals
    readonly domain=${1:-}
    readonly env=${2:-}

    # Validate & load secrets
    if ! _iac_load_env no_env; then
        show_help create
        return 1
    fi

    _iac_create_validate || return 1

    if ! _iac_create_execute ; then
        echo
        echo "** Creation failed!!"
        echo "   No cleanup performed to assist diagnostics..."
        echo
        return 1
    fi
}

_iac_createdomain_validate() {
    ## Validates the inputs when creating a new domain.
    ##
    ## Inputs: (globals)
    ##  domain
    ##  domain_seed
    ##  srcsecret_path    (the original path to the secrets)

    local arg_error=""


    # Validation Phase 1: Syntax
    if [ -z "${domain}" ]; then
        arg_error="  Missing DOMAIN: Must specify domain (as a DNS suffix)\n"
    fi
    if [ -z "${domain_seed}" ]; then
        arg_error="${arg_error}  Missing DOMAIN_SEED: Must specify the variables on which to base this domain\n"
    fi
    if [ -z "${srcsecret_path}" ]; then
        arg_error="${arg_error}  Missing SECRETS_FILE: Must specify secrets file\n"
    fi

    # Validation Phase 2: Semantic Errors - secrets
    if [ -z "${arg_error}" ]; then
        if ! _load_secrets "${srcsecret_path}"; then
            arg_error="  Could not load secret file: ${srcsecret_path}"
        fi
    fi

    # Validation Phase 2: Semantic Errors - meta
    if [ -z "${arg_error}" ]; then
        local base_env_path="${path_seeds}/${domain_seed}"

        # More checking
        if [ -d "${path_gardens}/${domain}" ]; then
            arg_error="  Environment '${domain}' already exists\n"
        fi

        # TODO: check that DOMAIN is 'under' the base domain's DNS

        if [ ! -d "${base_env_path}" ]; then
            arg_error="${arg_error}  Could not find domain template at ${base_env_path}\n"
        fi
    fi

    # Show error, show help and exit.
    if [ ! -z "${arg_error}" ]; then
        echo "ERROR:"
        echo -e "${arg_error}"
        show_help createdomain
        return 1
    fi
}


_iac_createdomain_execute() {
    ## Creates a new domain
    ##
    ## Inputs: (globals)
    ##  domain
    ##  domain_seed
    ##  srcsecret_path    (the original path to the secrets)
    ## Outputs:
    ##  If successful:
    ##    1. a new env/${domain}/ directory with appropriate vars/symlinks
    ##    2. a new core environment for the above domain (at env/${domain}/core)

    # sanity check
    [ -z ${domain} ] && echo "ERROR: No domain set" && return 1
    [ -z ${domain_path} ] && echo "ERROR: No domain_path set" && return 1

    local base_env_path="${path_seeds}/${domain_seed}"

    echo "New Domain:                 ${domain}     (Shortened to '${domain_short}')"
    if [ ${verbose} ]; then
        echo "    Domain Seed:            ${domain_seed}"
        echo "    AWS_DEFAULT_REGION:     ${AWS_DEFAULT_REGION}"
        echo "    AWS_ACCESS_KEY_ID:      ${AWS_ACCESS_KEY_ID:0:5}......${AWS_ACCESS_KEY_ID: -3}"
        echo "    AWS_SECRET_ACCESS_KEY:  ${AWS_SECRET_ACCESS_KEY:0:5}.........${AWS_SECRET_ACCESS_KEY: -3}"
    fi
    # TODO: add [y/N] confirmation

    # Start writing...
    mkdir ${domain_path} ${domain_path}/vars
    cp ${path_blueprints}/domain_templates/domain/*.tf ${domain_path}/vars/
    cp ${path_seeds}/${domain_seed}/*.tf* ${domain_path}/vars/

    # Read the domain template and populate with appropriate values
    # work out the 'domain_short' # TODO: Move somewhere sensible
    local domain_tfvars_template="${path_blueprints}/domain_templates/domain/domain.tfvars.template"
    local domain_tfvars_path="${domain_path}/vars/domain.tfvars"

    # sanity check (ie. safety code & shouldn't occur)
    [ ! -f ${domain_tfvars_path} ] && echo "ERROR: Could not find '${domain_tfvars_path}' to append to" && return 1

    # Version 1:
    # The commented perl works (it grabs the values from the environment), but much too clever. simplifying with more explicit sed's.
    # export domain domain_short
    # perl -p  -e 's/\$\{([^}]+)\}/defined ${ENV}{$1} ? ${ENV}{$1} : $&/eg' < ${domain_tfvars_template} >> ${domain_tfvars_path}

    # Version 2:
    # Much simpler, and doesn't need exports: Sed the explicit values from the template and append onto the domain.tfvars
    sed -e "s/__IAC_DOMAIN__/${domain}/" -e "s/__IAC_DOMAIN_SHORT__/${domain_short}/" ${domain_tfvars_template} >> ${domain_tfvars_path}

    # create the my_secrets link (relative symlink)
    ln -r -s ${srcsecret_path} ${domain_path}/my_secrets

    echo "Domain creation complete."

    ##
    ## Now create the CORE environment using _iac_create_execute()
    readonly env="core"  # set the global
    _iac_load_env "no_env"  || return 1     # load the globals for the env (explicitly state the env doesn't exist yet)
    _iac_create_execute     || return 1

    if [ ${verbose} ] && which tree > /dev/null 2>&1 ; then
        echo
        _iac_show_domain ${domain_path}
    fi

    echo
    _iac_todo_execute ${domain} || return 1
    return 0
}

iac_createdomain() {
    domain=${1:-}
    domain_seed=${2:-}
    srcsecret_path=${3:-}   # srcsecret_path differs from secret_path.
                            # The src is the origin path, the latter is a symlink used post-domain-creation

    # Special cases:
    #   with -l param:     list the available domain seeds
    #   without any args:  as -l + show help
    [[ "${domain}" == "-l" ]] && show_domain_seeds && return 0
    [[ -z "${domain}" ]] && show_help createdomain && echo && show_domain_seeds && return 1

    # Validate
    if ! _iac_load_domain no_domain; then
        show_help createdomain
        return 1
    fi

    # Validate args & load secrets
    _iac_createdomain_validate || return 1

    if ! _iac_createdomain_execute ; then
        echo
        echo "** Creation failed!!"
        echo "   No cleanup performed to assist diagnostics..."
        echo
        return 1
    fi
}


_iac_check_core_dependencies() {
    # There are dependencies between 'core' and all other environments.
    # Ensure that 'core' environment has been run first, otherwise a plan/apply on a devtest env will fail.
    # Fortunately, this is easy due to the symlink core.tfvars - just ensure there's a few lines in it
    echo "$(cat ${env_path}/core.tfvars)"
    if [ $(cat ${env_path}/core.tfvars | wc -l ) -gt 2 ]; then
        # Based on file-size alone, this looks okay....
        # TODO: Use a more rigorous check for core dependencies within sub-envs?
        return 0
    fi
    return 1
}





iac_plan() {
    if ! _iac_set_domainenv_from_cwd; then
        readonly domain=${1:-}
        readonly env=${2:-}
    fi

    _iac_load_env || return 1

    if [[ ${env_type} != "core" ]] && ! _iac_check_core_dependencies; then
        echo
        echo "ERROR: Cannot find core dependencies. This is probably due the core environment not yet being applied."
        echo "       Please 'apply ${domain} core' before continuing this operation."
        return 1
    fi

    cd "${env_path}"
    terraform get
    case ${env_type} in
        core)
            terraform plan -module-depth=-1 -var-file=domain.tfvars -var-file=env.tfvars || return 1
            ;;
        sub)
            terraform plan -module-depth=-1 -var-file=domain.tfvars -var-file=env.tfvars -var-file=core.tfvars || return 1
            ;;
    esac
    cd "${__IACDIR}"

}

function iac_apply() {
    if ! _iac_set_domainenv_from_cwd; then
        readonly domain=${1:-}
        readonly env=${2:-}
    fi

    _iac_load_env || return 1

    # refresh the modules before applying
    cd "${env_path}"
    terraform get
    # Sub and Core are mostly the same, with core needed some post-processing for output vars
    case "${env_type}" in
        core)
            local core_tfvars="../vars/core.tfvars"
            terraform apply -var-file=domain.tfvars -var-file=env.tfvars || return 1
            terraform output > "${core_tfvars}.raw"
            convert_to_tfvars "${core_tfvars}.raw" > "${core_tfvars}"
            ;;
        sub)
            terraform apply -var-file=domain.tfvars -var-file=env.tfvars -var-file=core.tfvars || return 1
            ;;
        *)
            echo "ERROR: Unknown environment type"
            return 1
            ;;
    esac
    cd "${__IACDIR}"

}


function iac_destroy() {
    if ! _iac_set_domainenv_from_cwd; then
        readonly domain=${1:-}
        readonly env=${2:-}
    fi

    _iac_load_env || { show_help destroy ; return 1; }

    # Append extra varfile for non-core envs
    varfiles="-var-file=domain.tfvars -var-file=env.tfvars"
    [[ "${env_type}" != "core" ]] && varfiles="${varfiles} -var-file=core.tfvars"

    cd "${env_path}"
    terraform destroy ${varfiles}
    #rm -rfv ${env_path}
    cd "${__IACDIR}"

}



function main() {
    # - if -d is the first argument, then bash tracing is turned on (very spammy)
    # - if -v is the first argument, then the ${verbose} global is set

    #
    # Bash debugging
    # - use -d as first arg
    #
    if [[ "${1:-}" = "-d" ]]; then
        set -o xtrace
        #export PS4='+$BASH_SOURCE:$LINENO:$FUNCNAME: '      # Super trace!
        local PS4_nocolor='$BASH_SOURCE:$LINENO:$FUNCNAME:  '
        export PS4="${cblue}${PS4_nocolor}${creset}"      # Super trace!
        shift 1
        #
        set | grep "^__"
    fi

    #
    # Terraform debugging
    # - use -t[tdiwe] as next arg
    #
    if [[ "${1:-}" = "-t"* ]]; then
        local level=""   arg_level="${1#-t}"   # bash magic to strip off '-t' prefix
        for L in t,TRACE d,DEBUG i,INFO w,WARN e,ERROR; do
            [[ "${arg_level}" = "${L%,*}" ]] && level="${L#*,}" && continue
        done
        [ -z ${level} ] && echo "Unknown terraform loglevel: '${arg_level}'. Try one of: t,d,i,w,e" && exit 1
        export TF_LOG="${level}"
        shift 1
    fi

    #
    # Verbose mode
    # - use -v as next arg
    #
    if [[ "${1:-}" = "-v" ]]; then
        verbose=1       # Sets the global
        shift 1
    fi

    # If no explicit TF_LOG declared, then use our own, sending to /tmp
    [ -z ${TF_LOG:-} ] && export TF_LOG="DEBUG" TF_LOG_PATH="/tmp/iac.log"

    # Always start by changing to the IAC directory. The original working directory is stored in ${__OWD}
    cd "${__IACDIR}"
    iac_command="${1:-}"
    case ${iac_command} in
        show)
            shift 1
            iac_show "$@"
            ;;
        createdomain)
            shift 1
            iac_createdomain "$@"
            ;;
        todo)
            shift 1
            iac_todo "$@"
            ;;
        create)
            shift 1
            iac_create "$@"
            ;;
        plan)
            shift 1
            iac_plan "$@"
            ;;
        apply)
            shift 1
            iac_apply "$@"
            ;;
        destroy)
            shift 1
            iac_destroy "$@"
            ;;
        custom)
            shift 1
            iac_custom_command "$@"
            ;;
        register)
            shift 1
            iac_register "$@"
            ;;
        help)
            shift 1
            # asking for 'help' always gives full help
            arg=${1:-help}
            show_help ${arg}
            ;;
        *)
            show_help short
            ;;
    esac
    return $?
}

main "$@"


