# IAC

## Synopsis

Looking for a quickstart doco? For instructions on how to use IAC, read _HOWTO.md_

The IAC Framework provides the ability to manage multiple sets of terraform environments.

It provides the user with a secure mechanism to share these environments with authorised users, avoiding
  placing any 'secrets' in an external repository (eg. git or S3)


## IAC Entities 

The key entities are: **domains** and **environments**. See diagram below. 

The IAC Framework can manage multiple sets of environments. A single managed set of environments is
called a **domain** (and has a matching DNS suffix. eg. _test1.devop5.io_).

A **domain** consists of multiple environments, one which is (always) called **core**. This contains shared services
such as a puppet server. The DNS-suffix for **core** is identical to the *domain*. (as before, _test1.devop5.io_). The shared
puppet service would thus be accessible via _puppet.test.devop5.io_.

A **domain** will have one (or probably more) additional **environments** (also known as sub-environments, or devtest 
environments). These are the environments that expose functionality to users, normally via multiple endpoints. They are 
preferably self-contained to avoid external dependencies breaking them (except for **core** of course). 

The relationship between the **domains**, **environments** and the DNS conventions are shown below:
 
    +-----------------------------------------------------------------------------------------+
    | DOMAIN: test.devop5.io                                                                  |
    |                                                                                         |
    |  +--------------------------------------+   +----------------------------------------+  |
    |  | ENVIRONMENT: core (test.devop5.io)   |   | ENVIRONMENT: qa1 (qa1.test.devop5.io)  |  |
    |  |                                      |   |                                        |  |
    |  |  +--------------------------------+  |   |   +--------------------------------+   |  |
    |  |  | puppet (puppet.test.devop5.io) |  |   |   | web  (web.qa1.test.devop5.io)  |   |  |
    |  |  +--------------------------------+  |   |   +--------------------------------+   |  |
    |  |                                      |   |                                        |  |
    |  +--------------------------------------+   |                                        |  |
    |                                             |   +--------------------------------+   |  |
    |   FQDN="HOSTNAME.ENVIRONMENT.DOMAIN"        |   | api  (api.qa1.test.devop5.io)  |   |  |
    |                                             |   +--------------------------------+   |  |
    |   Except 'core' environment, which is       |                                        |  |
    |   replaced with "".                         +----------------------------------------+  |
    |                                                                                         |
    |                                                                                         |
    |  +-----------------------------------------------------------------------------------+  |
    |  | ENVIRONMENT: qa2 (qa2.test.devop5.io)                                             |  |
    |  |                                                                                   |  |
    |  |  +-------------------------------------+        +---+                             |  |
    |  |  | web0 (web0.qa2.test.devop5.io)      +--------+   |                             |  |
    |  |  +-------------------------------------+        | E |                             |  |
    |  |                                                 | L +-----------O                 |  |
    |  |  +-------------------------------------+        | B |                             |  |
    |  |  | web1 (web1.qa2.test.devop5.io)      +--------+   |  www.qa2.test.devop5.io     |  |
    |  |  +-------------------------------------+        +---+                             |  |
    |  |                                                                                   |  |
    |  +-----------------------------------------------------------------------------------+  |
    |                                                                                         |
    +-----------------------------------------------------------------------------------------+



**Note** The IAC utility does not perform any of the actual `terraform`ing. It provides a framework
 to manage multiple environments on a single machine with a shared **tfstate**, and executes the 
 appropriate terraform 

The **domain** is provisioned within a (pre-existing) AWS VPC, with each **environment** within a terraform-provisioned
AWS Subnet.  It is possible to have multiple domains with a VPC. 

A future enhancement may include spinning up the AWS VPC but there is
additional iAM policy tweaking required which hasn't been terraform-scripted as yet.

The terraform state (tfstate) is contained within an s3-bucket for multi-user workflows, and any other changes should be
managed within git (eg. changing terraform module code or the main.tf definition)



## Folder Structure

  - **HOWTO.md** - primarily usage of IAC
  - **README.md** - primarily enhancement/maintenance of IAC
  - **iac** - the primary utility script that wraps everything else
  - **util/** - occasionally useful scripts
  - **defs/** - terraform modules
    - **base/** the *module* for the base/shared vm's (including the octopus and puppet servers)
    - **testenv/** the *module* for the basic est environment
    - **bootstrap/** contains bootstrap templates (eg. cloud-init/user-data) for instance provisioning.
    - **windows/** powershell scripts (to be moved into puppet modules)
  - **resources/** shared resources throughout the project (eg. *ssh/config* setups, or common files that are symlink'ed to)
  - **envs/** contains the **domains** which contain multiple **environments**
    Each directory under envs/ maps to a **domain** (and should be named with the DNS suffix)
  - **template_my_secrets**

## AWS

### Pre-requisites for using IAC

- **AWS iAM User**: A iAM user with write perms to the resources below is required. TODO: Specify the iAM policies.
- **VPC**: A pre-existing VPC is required
- **Route53**: A pre-existing (internal) Zone is required, with association to the VPC. It is preferable that the Route53 is the
DNS for the VPC, however conditional-forwarders can be used if required.



## AWS Infrastructure

### VPC

For example, We use a pre-generated VPC (named test.devop5.io) which has:
- a private Route53 DNS zone (on test.devop5.io)
- a VPC Peering Connection to our primary VPC (containing our VPN) with appropriate VPC Route Table rules to access the test VPC.


### Security / Security Groups

Although we are enclosed within a private subnet accessible only by VPN, security-groups with 0.0.0.0/0 are best avoided.
There is a terraform variable provided to use instead of 0/0.

For best practice, Security Groups can be used for fine-grained access control on Application/Port combinations. Design will
be required as there is a maximum of 5 SG's per EC2 instance. We use *windows_common* and *linux_common* security groups
to aggregate 'the usual' ports.

#### Usage of Security Groups for fine-grained control

Example 1: *Pulling* - Puppet Master is _pulled_ by the agents.
- **sg-puppet-agent**: is assigned to machines that are agents.
- The puppetmaster only allows traffic on port *8140* from machines with the puppet-agent-sg.

Example 2: *Pushing* - Octopus Server _pushes_ to tentacle machines.
- **sg-octopus-agent**: is assigned to machines that are tentacles.
- This SG only allows traffic on port *10933* from machines possessing the octopus-server-sg.

These Security Groups are defined for each iac-env, and cannot be shared by other iac-envs.

---

---

# Under this point needs to be updated for *IAC* utility




### Environment variables

The 'secrets' are intentionally not stored in Git.
Copy the file at *resources/set_terraform_env* to *../set_terraform_env*, and set the following
environmental vars:

../set_terraform_env:

    ....
    AWS_ACCESS_KEY_ID
    AWS_SECRET_ACCESS_KEY
    MY_ATLAS_TOK (not yet, but soon)

    DEVTEST_SUBNET_START
    DOMAIN_PREFIX
    ....


### DNS

The private DNS zone allows resolution of all names to the private-ip.

- If using vpn1.devop5.io (full vpn) then your main nameserver is probably `10.66.0.2` - the VPC's nameserver.
- If using vpn2.devop5.io (split-tunnel) then the VPN will *add* a `10.66.0.2` nameserver for nslookups.

TODO: reverse lookups?






## Appendix

### Policies

a **iAM user** has been created with appropriate policies associated with it (TODO: spec these out exactly). This includes:
  - ReadWrite to the domain's EC2 resources
  - ReadWrite to the domain's VPC resources
  - ReadWrite to the domain's S3 bucket
  - ReadWrite to the Route53 Zone


